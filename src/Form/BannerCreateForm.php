<?php

namespace Drupal\volta_banners\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\OpenDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\Element;
use Drupal\volta_banners\Entity\Banner;

/**
 * Class BannerCreateForm.
 */
class BannerCreateForm extends FormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'banner_create_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('banner');
    $bundleOptions = [];

    foreach ($bundles as $key => $bundle) {
      $bundleOptions[$key] = $bundle['label'];
    }
    $form['select_banner_type'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Select element'),
      '#options' => $bundleOptions,
      '#ajax' => [
        'callback' => '::iefChangeBannerForm',
        'wrapper' => 'create-banner-ief',
        'event' => 'change',
      ],
    ];
//
//    $form['#prefix'] = '<div id="create-banner-ief">';
//    $form['#suffix'] = '</div>';


//    $form['ief_wrapper']['ief'] = [
//      '#type' => 'inline_entity_form',
//      '#entity_type' => 'banner',
//      '#bundle' => $form_state->getValue('select_banner_type') ?: 'banner',
////        '#default_value' => $selectedBannerType,
//    ];


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      \Drupal::messenger()->addMessage($key.': '.($key === 'text_format' ? $value['value'] : $value));
    }
  }

  public function iefChangeBannerForm(array &$form, FormStateInterface $form_state)
  {
    $banner = Banner::create(['type' => $form_state->getValue('select_banner_type') ?: 'banner']);
    $form = \Drupal::service('entity.form_builder')->getForm($banner, 'default');

    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('banner');
    $bundleOptions = [];

    foreach ($bundles as $key => $bundle) {
      $bundleOptions[$key] = $bundle['label'];
    }

    $form['select_banner_type'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Select element'),
      '#options' => $bundleOptions,
      '#ajax' => [
        'callback' => '::iefChangeBannerForm',
        'wrapper' => 'create-banner-ief',
        'event' => 'change',
      ],
    ];

//    $form['#prefix'] = '<div id="create-banner-ief">';
//    $form['#suffix'] = '</div>';

    $form["actions"]["submit"] = [
      '#type' => 'button',
      '#value' => 'Save me please',
      '#attributes' => ['class' => ['use-ajax']],
      '#ajax' => [
        'callback' => 'Drupal\volta_banners\Form\BannerForm::_customAjaxSubmit',
        'event' => 'click',
      ],
    ];

    return $form;
  }



}
