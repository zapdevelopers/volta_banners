<?php

namespace Drupal\volta_banners\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entity_browser\Element\EntityBrowserElement;
use Drupal\volta_banners\Entity\Banner;
use Drupal\volta_banners\Entity\BannerBlockBanner;

/**
 * Class BannerBlockForm.
 */
class BannerBlockForm extends FormBase
{
  private $uniqueId;

  /**
   * BannerBlockForm constructor.
   */
  public function __construct()
  {
    $this->uniqueId = Html::getUniqueId('banners');
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'banner_block_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $block_id = null)
  {

    $user_input = $form_state->getUserInput();

    $form['unique_id'] = [
      '#type' => 'hidden',
    ];
    if (!(isset($user_input['unique_id']))) {
      $form['unique_id']['#value'] = $this->uniqueId;
    } else {
      $unique_id = $user_input['unique_id'];
    }

    $hidden_id = 'target-id-' . $this->uniqueId;
    $details_id = 'banners-' . $this->uniqueId;


    // Algemene wrapper voor volgende element
    //// TODO: Denk dat dit weg mag
    $form['banner_form'] = [
      '#type' => 'container',
      '#description' => t('Select a custom set of banners that will be shown for this page only'),
      '#tree' => false,
      '#open' => true,
    ];

    /**
     * ID van bannerblock waarin banner actief is'
     **/
    $form['block_id'] = [
      '#type' => 'hidden',
      '#value' => $block_id,
    ];


    /**
     * Genest entity browser formulier om banners toe te voegen aan Banner Block
     */
    $form['banner_form']['banner_browser_link'] = [
      '#type' => 'entity_browser',
      '#entity_browser' => 'banner_browser',
      '#custom_hidden_id' => $hidden_id,
      '#cardinality' => 10,
      '#selection_mode' => EntityBrowserElement::SELECTION_MODE_APPEND,
      '#entity_browser_validators' => ['entity_type' => ['type' => 'banner']],
      '#default_value' => [],
      '#process' => [
        [
          '\Drupal\entity_browser\Element\EntityBrowserElement',
          'processEntityBrowser',
        ],
        [
          'Drupal\entity_browser\Plugin\Field\FieldWidget\EntityReferenceBrowserWidget',
          'processEntityBrowser',
        ],
      ],
    ];

    $form['banner_form']['banner_create_link'] = [
      '#type' => 'link',
      '#title' => $this->t('create'),
      "#url" => Url::fromRoute("entity.banner.add_form", ['banner_type' => 'banner']),
      "#attributes" => [
        'class' => ['use-ajax', 'btn', 'btn-primary'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 800]),
      ]
    ];

    /**
     * Form element waarin de BannerBlockBanners gaan zitten => lijst met weight en 'useDafault'
     * Heeft id om als wrapper te gebruiken. Aan de hand van deze id kan hij events opvangen uit entity browser die genest zit in dit form element
     **/
    $form['banner_form']['banners'] = [
      '#attributes' => ['class' => ['banner-wrapper']],
      '#id' => $details_id,
      '#type' => 'container',
      '#open' => 'true',
      //Values in form zijn genest
      '#tree' => true,
      'target_id' => [
        '#type' => 'hidden',
        '#id' => $hidden_id,
        '#attributes' => ['id' => $hidden_id],
        '#default_value' => [],
        '#ajax' => [
          'callback' => [$this, 'updateWidgetCallback'],
          'wrapper' => $details_id,
          'event' => 'entity_browser_value_updated',
        ],
      ],
    ];



    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];


    $form['banner_form']['banners'] = [
      '#type' => 'table',
      '#header' => array(
        $this
          ->t('Name'),
        $this
          ->t('Edit'),
        $this
          ->t('Delete')
      )
    ];


    /**
     * Actieve banners voor huidig bannerblok inladen
     */
    $bannerBlockBanners = $this->getBannerBlockBanners($form, $form_state);
    /** @var BannerBlockBanner $bannerBlockBanner */
    foreach ($bannerBlockBanners as $key => $bannerBlockBanner) {

      $test = $bannerBlockBanner->banner->first()->target_id;
      if ($bannerBlockBanner->get('banner')->first()->get('entity')->getTarget()) {
        /** @var Banner $banner */
        $banner = $bannerBlockBanner
          ->get('banner')
          ->first()
          ->get('entity')
          ->getTarget()
          ->getValue();



        $form['banner_form']['banners'][$key]['name'] = [
          '#type' => 'html_tag',
          '#tag' => 'strong',
          '#value' => $banner->getName(),
        ];
        $form['banner_form']['banners'][$key]['edit'] = [
          '#type' => 'link',
          '#title' => $this->t('edit'),
          "#url" => Url::fromRoute("entity.banner.edit_form", ['banner' => $banner->id(), 'block_id' => $block_id]),
          "#attributes" => [
            'class' => ['use-ajax', 'btn', 'btn-primary'],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode(['width' => 800]),
          ]
        ];
        $form['banner_form']['banners'][$key]['delete'] = [
          '#type' => 'link',
          '#title' => $this->t('delete'),
          "#url" => Url::fromRoute("entity.banner.delete_form", ['banner' => $banner->id()]),
          "#attributes" => [
            'class' => ['use-ajax', 'btn', 'btn-primary'],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode(['width' => 800]),
          ]
        ];
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    foreach ($form_state->getValues() as $key => $value) {
      // @TODO: Validate fields.
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
//    // Display result.
//    foreach ($form_state->getValues() as $key => $value) {
//      \Drupal::messenger()->addMessage($key . ': ' . ($key === 'text_format' ? $value['value'] : $value));
//    }
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return AjaxResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateWidgetCallback(array &$form, FormStateInterface $form_state)
  {
    $response = new AjaxResponse();

    /**
     * Banner id's uit de entity browser
     */
    if ($form_state->hasValue('banners') && $banners = $form_state->getValue('banners')['target_id']) {
      $bannerIds = array_map(
        function ($bannerString) {
          return explode(':', $bannerString)[1];
        }, explode(' ', $banners)
      );
    }

    $this->createBannerBlockBanner($bannerIds, $form_state->getValue('block_id'));
    $this->submitForm($form, $form_state);
    $response->addCommand(new ReplaceCommand('#nona', '<h1>Nona</h1>'));
    return $response;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  private function getBannerBlockBanners(array &$form, FormStateInterface $form_state)
  {
    $query = \Drupal::entityQuery('banner_block_banner');
    $query->condition('banner_block_id', $form['block_id']['#value'], '=');
    $bannerBlockBannerIds = $query->execute();

    return BannerBlockBanner::loadMultiple($bannerBlockBannerIds);
  }

  /**
   * @param $bannerIds
   * @param $blockId
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createBannerBlockBanner($bannerIds, $blockId)
  {
    /**
     * BlockBannerBlock entity aanmaken
     */
    foreach ($bannerIds as $bannerId) {
      //Query checken om te zien of BannerBlockBanner al bestaat
      $query = \Drupal::entityQuery('banner_block_banner');
      $query->condition('banner', $bannerId, '=');
      $query->condition('banner_block_id', $blockId, '=');
      $bannerBlockBannerIds = $query->execute();

      if (!count($bannerBlockBannerIds)) {
        $bannerBlockBanner = BannerBlockBanner::create([]);
        $bannerBlockBanner->set('banner', $bannerId);
        $bannerBlockBanner->set('banner_block_id', $blockId);
        $bannerBlockBanner->save();
      }
    }
  }
}
