<?php

namespace Drupal\volta_banners\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\OpenDialogCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\volta_banners\Entity\Banner;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Form controller for Banner edit forms.
 *
 * @ingroup volta_banners
 */
class BannerForm extends ContentEntityForm
{

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /* @var \Drupal\volta_banners\Entity\Banner $entity */

    $form['original_form'] =  parent::buildForm($form, $form_state);

    $form['original_form']['#prefix'] = '<div id="original-form">';
    $form['original_form']['#suffix'] = '</div>';

    unset($form['original_form']['actions']);

    $form_state->setCached(FALSE);

    if (\Drupal::request()->query->get('block_id')) {
      $form['block_id'] = [
        '#type' => 'hidden',
        '#value' => \Drupal::request()->query->get('block_id'),
      ];
    }

    $bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo('banner');
    $bundleOptions = [];

    foreach ($bundles as $key => $bundle) {
      $bundleOptions[$key] = $bundle['label'];
    }

    $form['select_banner_bundle'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Select element'),
      '#options' => $bundleOptions,
      '#ajax' => [
        'callback' => '::onSelectBannerBundle',
        'wrapper' => 'original-form',
        'event' => 'change',
        'method' => 'replace'
      ],
    ];

    $form["actions"]["submit"] = [
      '#type' => 'submit',
      '#value' => 'Save me please',
      '#ajax' => [
        'callback' => '::_customAjaxSubmit',
        //TODO: Moet wrapper hier nog wel bij?
        'wrapper' => 'original-form',
        'event' => 'click',
      ],
    ];

    return $form;
  }



  public function onSelectBannerBundle(array &$form, FormStateInterface $form_state)
  {
    $banner = Banner::create(['type' => $form_state->getValue('select_banner_bundle') ? : 'banner']);
    $form = \Drupal::service('entity.form_builder')->getForm($banner, 'default');

    //TODO: Kzn dit zonder ajax response?
    $ajax_response = new AjaxResponse();
    $ajax_response->addCommand(new HtmlCommand('#original-form', $form['original_form']));
    return $ajax_response;
  }


  public function _customAjaxSubmit($form, FormStateInterface $form_state)
  {
    $this->submitForm($form, $form_state);
    $this->save($form, $form_state);

    $title = "Hi, I'm a Dialog";
    $response = new AjaxResponse();
    $response->addCommand(new CloseDialogCommand());
    $block_id = $form_state->getValue('block_id');
    $modal_form = \Drupal::formBuilder()->getForm('Drupal\volta_banners\Form\BannerBlockForm', $block_id);

    $response->addCommand(new OpenDialogCommand('#some-element', null, $modal_form, ['width' => '700']));

    return $response;

  }


  public function save(array $form, FormStateInterface $form_state)
  {
    $entity = $this->entity;

    // Save as a new revision if requested to do so.
    if (!$form_state->isValueEmpty('new_revision') && $form_state->getValue('new_revision') != false) {
      $entity->setNewRevision();

      // If a new revision is created, save the current user as revision author.
      $entity->setRevisionCreationTime($this->time->getRequestTime());
      $entity->setRevisionUserId($this->account->id());
    } else {
      $entity->setNewRevision(false);
    }

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Banner.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Banner.', [
          '%label' => $entity->label(),
        ]));
    }
//    $form_state->setRedirect('entity.banner.canonical', ['banner' => $entity->id()]);
  }
}
