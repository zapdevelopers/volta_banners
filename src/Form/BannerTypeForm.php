<?php

namespace Drupal\volta_banners\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BannerTypeForm.
 */
class BannerTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $banner_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $banner_type->label(),
      '#description' => $this->t("Label for the Banner type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $banner_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\volta_banners\Entity\BannerType::load',
      ],
      '#disabled' => !$banner_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $banner_type = $this->entity;
    $status = $banner_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Banner type.', [
          '%label' => $banner_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Banner type.', [
          '%label' => $banner_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($banner_type->toUrl('collection'));
  }

}
