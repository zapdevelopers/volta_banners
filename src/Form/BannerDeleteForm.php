<?php

namespace Drupal\volta_banners\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Banner entities.
 *
 * @ingroup volta_banners
 */
class BannerDeleteForm extends ContentEntityDeleteForm {


}
