<?php

namespace Drupal\volta_banners\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Banner block banner entities.
 *
 * @ingroup volta_banners
 */
class BannerBlockBannerDeleteForm extends ContentEntityDeleteForm {


}
