<?php

namespace Drupal\volta_banners;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for banner_block_banner.
 */
class BannerBlockBannerTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
