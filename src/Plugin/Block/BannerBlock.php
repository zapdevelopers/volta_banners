<?php

namespace Drupal\volta_banners\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a 'BannerBlock' block.
 *
 * @Block(
 *  id = "banner_block",
 *  admin_label = @Translation("Banner block"),
 * )
 */
class BannerBlock extends BlockBase
{
  /**
   * {@inheritdoc}
   */
  public function build()
  {

    $configuration = $this->configuration;
    $block_id = $configuration['block_id'];

    $build = [];
    $build['#theme'] = 'banner_block';
    $build['#content']['edit_banner_button'] = [
      '#type'=>'link',
      '#title'=>'Edit banner block',
      "#url" => Url::fromRoute('volta_banners.banner_block_form', ['block_id'=> $block_id]),
      "#attributes" => [
        'class' => ['use-ajax', 'btn', 'btn-primary'],
        'data-dialog-type' => 'modal',
        'data-dialog-options' => Json::encode(['width' => 800]),
      ]
    ];

    $build['#attached']['library'][] = 'core/drupal.dialog.ajax';
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);
    return $form;
  }

  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $this->configuration['block_id'] = $form['id']['#value'];
  }
}
