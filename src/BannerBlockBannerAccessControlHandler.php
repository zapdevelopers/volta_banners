<?php

namespace Drupal\volta_banners;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Banner block banner entity.
 *
 * @see \Drupal\volta_banners\Entity\BannerBlockBanner.
 */
class BannerBlockBannerAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\volta_banners\Entity\BannerBlockBannerInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished banner block banner entities');
        }


        return AccessResult::allowedIfHasPermission($account, 'view published banner block banner entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit banner block banner entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete banner block banner entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add banner block banner entities');
  }


}
