<?php

namespace Drupal\volta_banners;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Banner block banner entities.
 *
 * @ingroup volta_banners
 */
class BannerBlockBannerListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Banner block banner ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\volta_banners\Entity\BannerBlockBanner $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.banner_block_banner.edit_form',
      ['banner_block_banner' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
