<?php

namespace Drupal\volta_banners\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Banner type entities.
 */
interface BannerTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
