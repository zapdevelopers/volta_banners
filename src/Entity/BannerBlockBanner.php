<?php

namespace Drupal\volta_banners\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Banner block banner entity.
 *
 * @ingroup volta_banners
 *
 * @ContentEntityType(
 *   id = "banner_block_banner",
 *   label = @Translation("Banner block banner"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\volta_banners\BannerBlockBannerListBuilder",
 *     "views_data" = "Drupal\volta_banners\Entity\BannerBlockBannerViewsData",
 *     "translation" = "Drupal\volta_banners\BannerBlockBannerTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\volta_banners\Form\BannerBlockBannerForm",
 *       "add" = "Drupal\volta_banners\Form\BannerBlockBannerForm",
 *       "edit" = "Drupal\volta_banners\Form\BannerBlockBannerForm",
 *       "delete" = "Drupal\volta_banners\Form\BannerBlockBannerDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\volta_banners\BannerBlockBannerHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\volta_banners\BannerBlockBannerAccessControlHandler",
 *   },
 *   base_table = "banner_block_banner",
 *   data_table = "banner_block_banner_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer banner block banner entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/banner_block_banner/{banner_block_banner}",
 *     "add-form" = "/admin/structure/banner_block_banner/add",
 *     "edit-form" = "/admin/structure/banner_block_banner/{banner_block_banner}/edit",
 *     "delete-form" = "/admin/structure/banner_block_banner/{banner_block_banner}/delete",
 *     "collection" = "/admin/structure/banner_block_banner",
 *   },
 *   field_ui_base_route = "banner_block_banner.settings"
 * )
 */
class BannerBlockBanner extends ContentEntityBase implements BannerBlockBannerInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    //TODO: Displayoptions weghalen
    $fields['is_default'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Is default'))
      ->setDescription(t('A boolean indicating whether the Banner is a default banner for this block.'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 20,
        ]);

    //TODO: Required mag weg
    $fields['banner'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Banner ID'))
      ->setDescription(t('The banner ID, bannerblockbanner was created for.'))
      ->setSetting('target_type', 'banner')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
        //TODO: Ik denk dat displayoptions wegmogen, ook die configurables
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['banner_block_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Banner Block ID'))
      ->setDescription(t('The banner block ID, bannerblock where banner was added for.'))
      ->setTranslatable(TRUE);

    //TODO: Niet readonly maken
    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight'))
      ->setReadOnly(TRUE);

    //TODO: Lengte langer maken
    $fields['path'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Path'))
      ->setDescription(t('The path.'))
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
      ));

    //TODO: display options weghalen
    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Banner is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 20,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
