<?php

namespace Drupal\volta_banners\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Banner type entity.
 *
 * @ConfigEntityType(
 *   id = "banner_type",
 *   label = @Translation("Banner type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\volta_banners\BannerTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\volta_banners\Form\BannerTypeForm",
 *       "edit" = "Drupal\volta_banners\Form\BannerTypeForm",
 *       "delete" = "Drupal\volta_banners\Form\BannerTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\volta_banners\BannerTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "banner_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "banner",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "enabled",
 *     "bundle"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/banner_type/{banner_type}",
 *     "add-form" = "/admin/structure/banner_type/add",
 *     "edit-form" = "/admin/structure/banner_type/{banner_type}/edit",
 *     "delete-form" = "/admin/structure/banner_type/{banner_type}/delete",
 *     "collection" = "/admin/structure/banner_type"
 *   }
 * )
 */
class BannerType extends ConfigEntityBundleBase implements BannerTypeInterface {

  /**
   * The Banner type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Banner type label.
   *
   * @var string
   */
  protected $label;

}
