<?php

namespace Drupal\volta_banners\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Banner entities.
 *
 * @ingroup volta_banners
 */
interface BannerInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Banner name.
   *
   * @return string
   *   Name of the Banner.
   */
  public function getName();

  /**
   * Sets the Banner name.
   *
   * @param string $name
   *   The Banner name.
   *
   * @return \Drupal\volta_banners\Entity\BannerInterface
   *   The called Banner entity.
   */
  public function setName($name);

  /**
   * Gets the Banner creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Banner.
   */
  public function getCreatedTime();

  /**
   * Sets the Banner creation timestamp.
   *
   * @param int $timestamp
   *   The Banner creation timestamp.
   *
   * @return \Drupal\volta_banners\Entity\BannerInterface
   *   The called Banner entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Banner revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Banner revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\volta_banners\Entity\BannerInterface
   *   The called Banner entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Banner revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Banner revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\volta_banners\Entity\BannerInterface
   *   The called Banner entity.
   */
  public function setRevisionUserId($uid);

}
