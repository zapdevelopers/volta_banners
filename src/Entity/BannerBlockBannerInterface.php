<?php

namespace Drupal\volta_banners\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Banner block banner entities.
 *
 * @ingroup volta_banners
 */
interface BannerBlockBannerInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Banner block banner name.
   *
   * @return string
   *   Name of the Banner block banner.
   */
  public function getName();

  /**
   * Sets the Banner block banner name.
   *
   * @param string $name
   *   The Banner block banner name.
   *
   * @return \Drupal\volta_banners\Entity\BannerBlockBannerInterface
   *   The called Banner block banner entity.
   */
  public function setName($name);

  /**
   * Gets the Banner block banner creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Banner block banner.
   */
  public function getCreatedTime();

  /**
   * Sets the Banner block banner creation timestamp.
   *
   * @param int $timestamp
   *   The Banner block banner creation timestamp.
   *
   * @return \Drupal\volta_banners\Entity\BannerBlockBannerInterface
   *   The called Banner block banner entity.
   */
  public function setCreatedTime($timestamp);

}
