<?php

/**
 * @file
 * Contains banner_block_banner.page.inc.
 *
 * Page callback for Banner block banner entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Banner block banner templates.
 *
 * Default template: banner_block_banner.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_banner_block_banner(array &$variables) {
  // Fetch BannerBlockBanner Entity Object.
  $banner_block_banner = $variables['elements']['#banner_block_banner'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
